# 19th Century Artillery Simulation Game

Just an opportunity for learning Unreal Engine 4. It is a 19th century artillery simulation game, where you operate
cannons from the 1800s (currently only the m1857 12-pounder napoleon).

![](doc/screenshot.png)

I am still to work out the mechanics and, well... Pretty much everything. But I will figure things out as I go.

[Here is a video of the current state](https://youtu.be/FHZEBHYOFjw)

