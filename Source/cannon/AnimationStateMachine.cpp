#include "AnimationStateMachine.h"
#include "Kismet/KismetMathLibrary.h"

AnimationStateMachine::AnimationStateMachine() {
    idle = true;
}

void AnimationStateMachine::addMeshToState(int id, vector <MeshAnimState> mesh_state_vec) {
    for(auto mesh_state: mesh_state_vec) {
        if(mesh_state.mesh) {
            FRotator ct_rot =  mesh_state.mesh->GetRelativeRotation();
            FString repr = ct_rot.ToString();
        } else {
        }
    }

    if (states.find(id) == states.end()) {
        states.insert({id, mesh_state_vec});
        return;
    }
    auto state = states.at(id);
    state.insert(state.end(), mesh_state_vec.begin(), mesh_state_vec.end());
}

void AnimationStateMachine::addMeshToState(int id, MeshAnimState mesh_state) {
    if(mesh_state.mesh) {
        FRotator ct_rot =  mesh_state.mesh->GetRelativeRotation();
        FString repr = ct_rot.ToString();
    } else {
    }
    vector <MeshAnimState> single_state_vec = {mesh_state};
    addMeshToState(id, single_state_vec);
}

void AnimationStateMachine::transitionTo(int next_id) {
    GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, FString::Printf(TEXT("Transition to %d"), next_id));
    next_state.push_back({next_id, states.at(next_id)});
    idle = false;
}
void AnimationStateMachine::transitionTo(vector<int> ids) {
    for (int id: ids) {
        transitionTo(id);
    }
}

bool AnimationStateMachine::tick(float delta_time) {
    if (idle || next_state.empty()) return false;
    bool still_ticking = false;

    pair<int, vector<MeshAnimState>> state = next_state.front();
    for (auto mesh: state.second) {
        still_ticking |= tickMesh(mesh, delta_time);
    }
    if (!still_ticking) {
        current_state_id = next_state.front().first;
        next_state.erase(next_state.begin());
    }
    idle = next_state.empty();
    return idle;
}

bool AnimationStateMachine::tickMesh(MeshAnimState mesh_state, float delta_time) {
    bool still_ticking = false;
    pair<bool, FVector> locationOffset = getLocationOffset(mesh_state, delta_time);
    pair<bool, FRotator> rotationOffset = getRotationOffset(mesh_state, delta_time);

    mesh_state.mesh->AddRelativeLocation(locationOffset.second);
    mesh_state.mesh->AddRelativeRotation(rotationOffset.second);

    still_ticking |= locationOffset.first;
    still_ticking |= rotationOffset.first;

    return still_ticking;
}

pair<bool, FVector> AnimationStateMachine::getLocationOffset(MeshAnimState mesh_state, float delta) {
    FVector current = mesh_state.mesh->GetRelativeLocation();
    FVector target = mesh_state.loc;
    float velocity = delta * base_loc_vel * mesh_state.velocity;

    float dx = target.X - current.X;
    float dy = target.Y - current.Y;
    float dz = target.Z - current.Z;

    float dist = UKismetMathLibrary::Sqrt(dx*dx + dy*dy + dz*dz);
    float factor = std::min(1.0f, velocity / dist);

    FVector deltaLoc(dx * factor, dy * factor, dz * factor);

    bool not_finished_yet = dist > velocity;
    return {not_finished_yet, deltaLoc};
}

pair<bool, FRotator> AnimationStateMachine::getRotationOffset(MeshAnimState mesh_state, float delta) {
    FRotator current = mesh_state.mesh->GetRelativeRotation();
    FRotator target = mesh_state.rot;

    FString targetrep = target.ToString();
    FString currentrep = current.ToString();

    float velocity = delta * base_rot_vel * mesh_state.velocity;
    float d_pitch = target.Pitch - current.Pitch;
    float d_yaw = target.Yaw - current.Yaw;
    float d_roll = target.Roll - current.Roll;

    float dist = std::abs(d_pitch) + std::abs(d_yaw) + std::abs(d_roll);
    float factor = std::min(1.0f, velocity / dist);

    FRotator delta_rot(d_pitch * factor, d_yaw * factor, d_roll * factor);

    bool not_finished_yet = dist > velocity;
    return {not_finished_yet, delta_rot.Clamp()};
}
