#ifndef UE4_ANIMATIONSTATEMACHINE_H
#define UE4_ANIMATIONSTATEMACHINE_H
#include "Misc/Optional.h"

#include <map>
#include <vector>
#include <optional>

#include "CoreMinimal.h"

using std::map;
using std::vector;
using std::pair;

//class MeshAnimState {
//public:
//    MeshAnimState(UStaticMeshComponent* m, long vel){
//        if (!m) std::cout << "MeshAnimState invalid mesh\n";
//        else {
//            std::cout << "\n\n-------------| MeshAnimState |-------------\n";
//            std::cout << "has mesh\n";
//            FRotator ct_rot =  m->GetRelativeRotation();
//            std::cout << "got rotation\n";
//            FString repr = ct_rot.ToString();
//            std::cout << TCHAR_TO_UTF8(*repr) << "\n";
//            std::cout << "-------------------------------\n";
//        }
////        std::cout << " on MeshAnimState constructor > " << m->GetRelativeRotation().Pitch << " / " << m->GetRelativeRotation().Yaw << " / " << m->GetRelativeRotation().Roll <<  "\n";
//        mesh = m;
//        velocity = vel;
//    }
//    MeshAnimState(UStaticMeshComponent* m, long vel, FVector l) : loc(l) { MeshAnimState(m, vel); }
//    MeshAnimState(UStaticMeshComponent* m, long vel, FRotator r) : rot(r) { MeshAnimState(m, vel); }
//    MeshAnimState(UStaticMeshComponent* m, long vel, FVector l, FRotator r) : loc(l), rot(r)  { MeshAnimState(m, vel); }
//
//public:
//    long velocity;
//    UStaticMeshComponent *mesh;
//    TOptional<FVector> loc;
//    TOptional<FRotator> rot;
//};

struct MeshAnimState {
    UStaticMeshComponent *mesh;
    long velocity;
    FVector loc;
    FRotator rot;
};

class AnimationStateMachine {
public:

public:
    AnimationStateMachine();

    AnimationStateMachine(int initial_state) : current_state_id(initial_state) {};

    void addMeshToState(int id, MeshAnimState mesh_state);

    void addMeshToState(int id, vector<MeshAnimState> mesh_state_vec);

    void transitionTo(int id);

    void transitionTo(vector<int> id);

    bool tick(float delta_time);

    bool tickMesh(MeshAnimState mesh_state, float delta_time);

    pair<bool, FVector> getLocationOffset(MeshAnimState mesh_state, float delta);

    pair<bool, FRotator> getRotationOffset(MeshAnimState mesh_state, float delta);

    int getState() { return current_state_id; }

    bool isBusy() { return !idle; }

    void setState(int id) { current_state_id = id;}

private:
    bool idle;

    int current_state_id;

    vector<pair<int, vector<MeshAnimState>>> next_state;

    map<int, vector<MeshAnimState>> states;

    const float base_loc_vel = 50;

    const float base_rot_vel = 100;
};


#endif //UE4_ANIMATIONSTATEMACHINE_H
