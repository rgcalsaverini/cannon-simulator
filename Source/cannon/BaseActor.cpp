#include "BaseActor.h"


ABaseActor::ABaseActor(const FString &path) {
    PrimaryActorTick.bCanEverTick = true;
    base_ref_path = path;
}

void ABaseActor::BeginPlay() {
    Super::BeginPlay();

}

void ABaseActor::addMesh(const FString &name, FVector pos, FRotator rot, const FString &parent) {
    FString full_ref(base_ref_path);
    full_ref += name + TEXT(".") + name;

    ConstructorHelpers::FObjectFinder <UStaticMesh> asset(*full_ref);
    UE_LOG(LogClass, Log, TEXT("Added mesh %s (%s)"), *name, *full_ref);
    UStaticMeshComponent *mesh_component = CreateDefaultSubobject<UStaticMeshComponent>(*name);

    if (asset.Succeeded()) {
        mesh_component->SetStaticMesh(asset.Object);
        mesh_component->SetRelativeLocation(pos);
        mesh_component->SetRelativeRotation(rot);
        if (!parent.IsEmpty() && meshes.Contains(parent)) {
            mesh_component->AttachToComponent(
                    meshes[parent],
                    FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true)
            );
        } else {
            mesh_component->AttachToComponent(
                    RootComponent,
                    FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true)
            );
        }
        meshes.Add(name, mesh_component);
    }
}

void ABaseActor::hideMesh(const FString &name) {
    meshes[name]->SetVisibility(false);
};

void ABaseActor::showMesh(const FString &name) {
    meshes[name]->SetVisibility(true);
};

void ABaseActor::addParticle(const FString &name) {
    FString full_ref(base_ref_path);
    full_ref += name + TEXT(".") + name;

    ConstructorHelpers::FObjectFinder<UParticleSystem> asset(*full_ref);
    if (asset.Succeeded()) {
        particles.Add(name, asset.Object);
        UE_LOG(LogClass, Log, TEXT("Added particle %s (%s)"), *name, *full_ref);
    } else {
        UE_LOG(LogClass, Log, TEXT("FAILED to add particle %s (%s)"), *name, *full_ref);
    }
}


void ABaseActor::addSound(const FString &name) {
    FString full_ref(base_ref_path);
    full_ref += name + TEXT(".") + name;

    ConstructorHelpers::FObjectFinder<USoundCue> asset(*full_ref);
    if (asset.Succeeded()) {
        sounds.Add(name, asset.Object);
        UE_LOG(LogClass, Log, TEXT("Added sound %s (%s)"), *name, *full_ref);
    } else {
        UE_LOG(LogClass, Log, TEXT("FAILED to add sound %s (%s)"), *name, *full_ref);
    }
}


void ABaseActor::addMeshRotTransform(const FString &mesh_name,
                                     float pitch,
                                     float yaw,
                                     float roll,
                                     float speed,
                                     float lag) {
    TArray<float> axis(new float[]{lag, speed, pitch, 0, yaw, 0, roll, 0}, 8);
    TPair <FString, TArray<float>> item(mesh_name, axis);
    mesh_rotations.Add(last_idx, item);
    last_idx++;
}

void ABaseActor::addMeshLocTransform(const FString &mesh_name, float x, float y, float z, float speed, float lag) {
    TArray<float> axis(new float[]{lag, speed, x, 0, y, 0, z, 0}, 8);
    TPair <FString, TArray<float>> item(mesh_name, axis);
    mesh_translations.Add(last_idx, item);
    last_idx++;
}


void ABaseActor::startMeshTransformation() {
    transform_elapsed_total = 0;
    last_idx = 0;
    mesh_transformation_underway = true;
}

void ABaseActor::stopMeshTransformation() {
    mesh_rotations.Empty();
    mesh_transformation_underway = false;
}

void ABaseActor::Tick(float delta_time) {
    Super::Tick(delta_time);
    tickTransformation(delta_time);
}

void ABaseActor::tickTransformation(float delta_time) {
    if (!mesh_transformation_underway) return;

    transform_elapsed_total += delta_time;
    TArray <uint8_t> finished_rotations;
    TArray <uint8_t> finished_translations;

    for (auto &item: mesh_rotations) {
        int idx = item.Key;
        if (doMeshRotation(item.Value.Key, item.Value.Value)) finished_rotations.Push(idx);
    }

    for (auto &item: mesh_translations) {
        int idx = item.Key;
        if (doMeshTranslation(item.Value.Key, item.Value.Value)) finished_translations.Push(idx);
    }

    if (!mesh_transformation_underway) return;

    for (auto idx: finished_rotations) if (mesh_rotations.Contains(idx)) mesh_rotations.Remove(idx);
    for (auto idx: finished_translations) if (mesh_translations.Contains(idx)) mesh_translations.Remove(idx);
    mesh_transformation_underway = mesh_rotations.Num() > 0 || mesh_translations.Num() > 0;
}


bool ABaseActor::doMeshRotation(FString mesh_name, TArray<float> &values) {
    float lag = values[0];
    if (lag > transform_elapsed_total) return false;

    if (!meshes.Contains(mesh_name)) {
        UE_LOG(LogClass, Log, TEXT("ERROR: No mesh named %s, cannot transform"), *mesh_name);
        stopMeshTransformation();
        return false;
    }

    float speed = values[1];
    float tgt_pitch = values[2];
    float val_pitch = values[3];
    float tgt_yaw = values[4];
    float val_yaw = values[5];
    float tgt_roll = values[6];
    float val_roll = values[7];

    // Interpolate new angles and calculate movement deltas
    float new_pitch = FMath::FInterpTo(val_pitch, tgt_pitch, transform_elapsed_total, speed);
    float new_yaw = FMath::FInterpTo(val_yaw, tgt_yaw, transform_elapsed_total, speed);
    float new_roll = FMath::FInterpTo(val_roll, tgt_roll, transform_elapsed_total, speed);
    float dp = new_pitch - val_pitch;
    float dy = new_yaw - val_yaw;
    float dr = new_roll - val_roll;
    FRotator delta_rotation(dp, dy, dr);

    FTransform transform = meshes[mesh_name]->GetComponentTransform();
    FRotator current_rotation = meshes[mesh_name]->GetComponentRotation();
    transform.SetRotation(current_rotation.Quaternion());
    transform.ConcatenateRotation(delta_rotation.Quaternion());
    transform.NormalizeRotation();
    meshes[mesh_name]->SetWorldTransform(transform);

    values[3] = new_pitch;
    values[5] = new_yaw;
    values[7] = new_roll;

    return std::abs(dp) + std::abs(dy) + std::abs(dr) < 0.001;
}

bool ABaseActor::doMeshTranslation(FString mesh_name, TArray<float> &values) {
    float lag = values[0];
    if (lag > transform_elapsed_total) return false;

    if (!meshes.Contains(mesh_name)) {
        UE_LOG(LogClass, Log, TEXT("ERROR: No mesh named %s, cannot transform"), *mesh_name);
        stopMeshTransformation();
        return false;
    }

    float speed = values[1];
    float tgt_x = values[2];
    float val_x = values[3];
    float tgt_y = values[4];
    float val_y = values[5];
    float tgt_z = values[6];
    float val_z = values[7];

    // Interpolate new angles and calculate movement deltas
    float new_x = FMath::FInterpTo(val_x, tgt_x, transform_elapsed_total, speed);
    float new_y = FMath::FInterpTo(val_y, tgt_y, transform_elapsed_total, speed);
    float new_z = FMath::FInterpTo(val_z, tgt_z, transform_elapsed_total, speed);
    float dx = new_x - val_x;
    float dy = new_y - val_y;
    float dz = new_z - val_z;
    FVector delta_location(dx, dy, dz);

//    FVector current_location = meshes[mesh_name]->GetRelativeLocation();
    meshes[mesh_name]->AddRelativeLocation(delta_location);

    values[3] = new_x;
    values[5] = new_y;
    values[7] = new_z;

    return std::abs(dx) + std::abs(dy) + std::abs(dz) < 0.001;
}
