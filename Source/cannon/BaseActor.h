#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Particles/ParticleSystem.h"
#include "Sound/SoundCue.h"
#include "BaseActor.generated.h"

UCLASS()
class CANNON_API ABaseActor : public AActor {
	GENERATED_BODY()
	
public:	
    ABaseActor() : ABaseActor(TEXT("")) {  };

    ABaseActor(const FString &path);

    virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

    /* Adding meshes */
    void addMesh(const FString &name) { addMesh(name, FVector(0,0,0), FRotator(0, 0, 0), TEXT("")); }

    void addMesh(const FString &name, FVector pos) { addMesh(name, pos, FRotator(0, 0, 0), TEXT("")); }

    void addMesh(const FString &name, FString parent) { addMesh(name, FVector(0,0,0), FRotator(0, 0, 0), parent); }

    void addMesh(const FString &name, FVector pos, const FString &parent) { addMesh(name, pos, FRotator(0, 0, 0), parent); }

    void addMesh(const FString &name, FRotator rot, const FString &parent) { addMesh(name, FVector(0, 0, 0), rot, parent); }

    void addMesh(const FString &name,  FVector pos, FRotator rot) { addMesh(name, pos, rot, TEXT("")); }

    void addMesh(const FString &name, FVector pos, FRotator rot, const FString &parent);

    void hideMesh(const FString &name);

    void showMesh(const FString &name);

    /* Particles */

    void addParticle(const FString &name);

    /* Sounds */

    void addSound(const FString &name);

    /* Simple mesh movement */

    void addMeshRotTransform(const FString &mesh_name, float pitch, float yaw, float roll, float speed, float lag);

    void addMeshLocTransform(const FString &mesh_name, float x, float y, float z, float speed, float lag);

    void startMeshTransformation();

    void stopMeshTransformation();

    void tickTransformation(float delta_time);

    bool doMeshRotation(FString mesh_name, TArray<float> &values);

    bool doMeshTranslation(FString mesh_name, TArray<float> &values);

public:	

protected:
    /* Static meshes */
    UPROPERTY(VisibleAnywhere)
    FString base_ref_path = TEXT("");

    UPROPERTY(VisibleAnywhere)
    TMap<FString, UStaticMeshComponent*> meshes;

    UPROPERTY(VisibleAnywhere)
    TMap<FString, UParticleSystem*> particles;

    UPROPERTY(VisibleAnywhere)
    TMap<FString, USoundCue*> sounds;

    /* Mesh transformations */
    TMap<int, TPair<FString, TArray<float>>> mesh_rotations;
    TMap<int, TPair<FString, TArray<float>>> mesh_translations;

    int last_idx = 0;

    bool mesh_transformation_underway = false;

    float transform_elapsed_total = 0;

};
