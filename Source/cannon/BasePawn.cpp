#include "BasePawn.h"
#include "Math/UnrealMathUtility.h"


ABasePawn::ABasePawn(const FString &path) {
    base_ref_path = path;
}

void ABasePawn::addMesh(const FString &name, FVector pos, FRotator rot, const FString &parent) {
    FString full_ref(base_ref_path);
    full_ref += name + TEXT(".") + name;

    ConstructorHelpers::FObjectFinder <UStaticMesh> asset(*full_ref);
    UE_LOG(LogClass, Log, TEXT("Added mesh %s (%s)"), *name, *full_ref);
    UStaticMeshComponent *mesh_component = CreateDefaultSubobject<UStaticMeshComponent>(*name);

    if (asset.Succeeded()) {
        mesh_component->SetStaticMesh(asset.Object);
        mesh_component->SetRelativeLocation(pos);
        mesh_component->SetRelativeRotation(rot);
        if (!parent.IsEmpty() && meshes.Contains(parent)) {
            mesh_component->AttachToComponent(
                    meshes[parent],
                    FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true)
            );
        } else {
            mesh_component->AttachToComponent(
                    RootComponent,
                    FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true)
            );
        }
        meshes.Add(name, mesh_component);
    }
}

void ABasePawn::hideMesh(const FString &name) {
    meshes[name]->SetVisibility(false);
};

void ABasePawn::showMesh(const FString &name) {
    meshes[name]->SetVisibility(true);
};

void ABasePawn::addMeshRotTransform(const FString &mesh_name,
                                    float pitch,
                                    float yaw,
                                    float roll,
                                    float speed,
                                    float lag) {
    TArray<float> axis(new float[]{lag, speed, pitch, 0, yaw, 0, roll, 0}, 8);
    TPair <FString, TArray<float>> item(mesh_name, axis);
    mesh_rotations.Add(item);
}

void ABasePawn::startMeshTransformation() {
    transform_elapsed_total = 0;
    mesh_transformation_underway = true;
}

void ABasePawn::stopMeshTransformation() {
    mesh_rotations.Empty();
    mesh_transformation_underway = false;
}

void ABasePawn::Tick(float delta_time) {
    Super::Tick(delta_time);
    tickTransformation(delta_time);
}

void ABasePawn::tickTransformation(float delta_time) {
    if (!mesh_transformation_underway) return;

    transform_elapsed_total += delta_time;
    TArray<uint8_t> finished_rotations;
    uint8_t idx = 0;

    for (auto &rotation: mesh_rotations) {
        if (doMeshRotation(rotation.Key, rotation.Value)) finished_rotations.Push(idx);
        idx++;
    }

    if (!mesh_transformation_underway) return;

    for (auto rot_idx: finished_rotations) {
        mesh_rotations.RemoveAt(rot_idx);
    }

    mesh_transformation_underway = mesh_rotations.Num() > 0;
}


bool ABasePawn::doMeshRotation(FString mesh_name, TArray<float> &values) {
    float lag = values[0];
    if (lag > transform_elapsed_total) return false;

    if (!meshes.Contains(mesh_name)) {
        UE_LOG(LogClass, Log, TEXT("ERROR: No mesh named %s, cannot transform"), *mesh_name);
        stopMeshTransformation();
        return false;
    }

    float speed = values[1];
    float tgt_pitch = values[2];
    float val_pitch = values[3];
    float tgt_yaw = values[4];
    float val_yaw = values[5];
    float tgt_roll = values[6];
    float val_roll = values[7];

    // Interpolate new angles and calculate movement deltas
    float new_pitch = FMath::FInterpTo(val_pitch, tgt_pitch, transform_elapsed_total, speed);
    float new_yaw = FMath::FInterpTo(val_yaw, tgt_yaw, transform_elapsed_total, speed);
    float new_roll = FMath::FInterpTo(val_roll, tgt_roll, transform_elapsed_total, speed);
    float dp = new_pitch - val_pitch;
    float dy = new_yaw - val_yaw;
    float dr = new_roll - val_roll;
    FRotator delta_rotation(dp, dy, dr);

    FTransform transform = meshes[mesh_name]->GetComponentTransform();
    FRotator current_rotation = meshes[mesh_name]->GetComponentRotation();
    transform.SetRotation(current_rotation.Quaternion());
    transform.ConcatenateRotation(delta_rotation.Quaternion());
    transform.NormalizeRotation();
    meshes[mesh_name]->SetWorldTransform(transform);

    values[3] = new_pitch;
    values[5] = new_yaw;
    values[7] = new_roll;

    return std::abs(dp) + std::abs(dy) + std::abs(dr) < 0.001;
}

//FQuat SomeQuat
//FRotator = SomeQuat.Rotator()
//
//FRotator SomeRot
//FQuat = SomeRot.Quaternion()