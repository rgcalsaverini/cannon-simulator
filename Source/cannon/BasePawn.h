#pragma once

#include "CoreMinimal.h"
#include "Containers/Array.h"
#include "BasePawn.generated.h"

UCLASS()
class CANNON_API ABasePawn : public APawn{
	GENERATED_BODY()

protected:
	ABasePawn() : ABasePawn(TEXT("")) {  };

	ABasePawn(const FString &path);

protected:
    /* Adding meshes */
    void addMesh(const FString &name) { addMesh(name, FVector(0,0,0), FRotator(0, 0, 0), TEXT("")); }

    void addMesh(const FString &name, FVector pos) { addMesh(name, pos, FRotator(0, 0, 0), TEXT("")); }

    void addMesh(const FString &name, FString parent) { addMesh(name, FVector(0,0,0), FRotator(0, 0, 0), parent); }

    void addMesh(const FString &name, FVector pos, const FString &parent) { addMesh(name, pos, FRotator(0, 0, 0), parent); }

    void addMesh(const FString &name, FRotator rot, const FString &parent) { addMesh(name, FVector(0, 0, 0), rot, parent); }

    void addMesh(const FString &name,  FVector pos, FRotator rot) { addMesh(name, pos, rot, TEXT("")); }

    void addMesh(const FString &name, FVector pos, FRotator rot, const FString &parent);

    void hideMesh(const FString &name);

    void showMesh(const FString &name);

    /* Simple mesh movement */
    void addMeshRotTransform(const FString &mesh_name, float pitch, float yaw, float roll, float speed, float interval);

    void startMeshTransformation();

    void stopMeshTransformation();

    void tickTransformation(float delta_time);

    bool doMeshRotation(FString mesh_name, TArray<float> &values);

    /* Other */

    virtual void Tick(float delta_time) override;

protected:
    /* Static meshes */
    UPROPERTY(VisibleAnywhere)
    FString base_ref_path = TEXT("");

    UPROPERTY(VisibleAnywhere)
    TMap<FString, UStaticMeshComponent*> meshes;

    /* Mesh transformations */

    TArray<TPair<FString, TArray<float>>> mesh_rotations;

    bool mesh_transformation_underway = false;

    float transform_elapsed_total = 0;
};
