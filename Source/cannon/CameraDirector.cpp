#include "CameraDirector.h"
#include <iostream>

CameraDirector::CameraDirector(double spd, FVector loc, FRotator rot, double field) {
    speed = spd * CameraDirector::base_speed;
    fov_speed = spd * CameraDirector::base_fov_speed;
    rot_speed = spd * CameraDirector::base_rot_speed;
    location = loc;
    rotation = rot;
    fov = field;
}

bool CameraDirector::goTo(UCameraComponent *camera, float delta, FVector offset) {
    pair<bool, FVector> locationOffset = getLocationOffset(camera, delta, offset);
    pair<bool, FRotator> rotationOffset = getRotationOffset(camera, delta);
    pair<bool, float > fovOffset = getFovOffset(camera, delta);
    camera->AddRelativeLocation(locationOffset.second);
    camera->AddRelativeRotation(rotationOffset.second);
    camera->FieldOfView += fovOffset.second;
    return locationOffset.first || fovOffset.first || rotationOffset.first ;
}


void CameraDirector::start() {
    elapsed = 0;
}

pair<bool, FVector> CameraDirector::getLocationOffset(UCameraComponent *camera, float delta, FVector offset) {
    FVector current = camera->GetRelativeLocation();
    float periodXY = delta * speed;
    float periodZ = delta * speed;
    float dx = location.X + offset.X- current.X;
    float dy = location.Y + offset.Y - current.Y;
    float dz = location.Z + offset.Z - current.Z;
    FVector deltaLoc(
            dx > 0 ? std::min(dx, periodXY) : std::max(dx, -periodXY),
            dy > 0 ? std::min(dy, periodXY) : std::max(dy, -periodXY),
            dz > 0 ? std::min(dz, periodZ) : std::max(dz, -periodZ)
    );
    bool not_finished_yet = (
            std::abs(dx) > periodXY
            || std::abs(dy) > periodXY
            || std::abs(dz) > periodZ
    );
    return {not_finished_yet, deltaLoc};
}

pair<bool, FRotator> CameraDirector::getRotationOffset(UCameraComponent *camera, float delta) {
    FRotator current = camera->GetRelativeRotation();
    float period = delta * rot_speed;
    float d_pitch = rotation.Pitch - current.Pitch;
    float d_yaw = rotation.Yaw - current.Yaw;
    float d_roll = rotation.Roll - current.Roll;

    FRotator deltaRot(
            d_pitch > 0 ? std::min(d_pitch, period) : std::max(d_pitch, -period),
            d_yaw > 0 ? std::min(d_yaw, period) : std::max(d_yaw, -period),
            d_roll > 0 ? std::min(d_roll, period) : std::max(d_roll, -period)
    );
    bool not_finished_yet = (
            std::abs(d_pitch) > period
            || std::abs(d_yaw) > period
            || std::abs(d_roll) > period
    );
    return {not_finished_yet, deltaRot};
}

pair<bool, float> CameraDirector::getFovOffset(UCameraComponent *camera, float delta) {
    float period = delta * fov_speed;
    float current = camera->FieldOfView;
    float d_fov = fov - current;
    float d_fov_step = std::min(std::max(d_fov, -period), period);
    return {std::abs(d_fov) > period, d_fov_step};
}