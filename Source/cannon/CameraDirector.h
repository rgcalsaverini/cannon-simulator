#ifndef UE4_CAMERADIRECTOR_H
#define UE4_CAMERADIRECTOR_H

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"

using std::pair;

class CameraDirector {
public:
    CameraDirector(double speed, FVector location, FRotator rotation, double fov);

    void start();

    bool goTo(UCameraComponent* camera, float delta) { return goTo(camera, delta, FVector(0.0f, 0.0f, 0.0f));}

    bool goTo(UCameraComponent* camera, float delta, FVector offset);

private:
    pair<bool, FVector> getLocationOffset(UCameraComponent* camera, float delta, FVector offset);

    pair<bool, FRotator> getRotationOffset(UCameraComponent* camera, float delta);

    pair<bool, float> getFovOffset(UCameraComponent* camera, float delta);

    static constexpr double base_speed = 300;
    static constexpr double base_fov_speed = 50;
    static constexpr double base_rot_speed = 100;

public:
    double elapsed;
    double speed;
    double fov_speed;
    double rot_speed;
    FVector location;
    FRotator rotation;
    double fov;
};


#endif //UE4_CAMERADIRECTOR_H
