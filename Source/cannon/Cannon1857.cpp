#include "Cannon1857.h"
#include "Shell12Normal.h"
#include "characters/RegularCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"


string DBGSTR(FString str) {
    string res = "";
    for (int i = 0; i < str.Len(); i++) {
        res += (char) ((*str)[i]);
    }
    return res;
}

// Sets default values
ACannon1857::ACannon1857() : ABaseActor(TEXT("/Game/cannon/m1857/")) {
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.

    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

    addMesh(TEXT("m1857_stock"), FVector(-90.0f, 0.0f, 0.0f));
    addMesh(TEXT("m1857_elevation_screw"), FVector(19.0f, 0.0f, 23.0f), TEXT("m1857_stock"));
    addMesh(TEXT("m1857_cannon"), FVector(86.98f, 0.0f, 48.68f), TEXT("m1857_stock"));
    addMesh(TEXT("m1857_wheel"), FVector(-3.0f, 0.0f, 0.0f));
    addMesh(TEXT("m1857_sight"), FVector(-79.03f, 0.0f, 13.19f), TEXT("m1857_cannon"));
    addMesh(TEXT("m1857_strike_pin"), FVector(-59.5f, -0.9f, 16.0f), TEXT("m1857_cannon"));

    hideMesh(TEXT("m1857_sight"));
    hideMesh(TEXT("m1857_strike_pin"));

    addParticle("P_c_muzzle_flash");

    addSound("cue_explosion");
    addSound("cue_wheel");

    addActionArea(TEXT("move"), FVector(-495.0f, 0.0f, 10.0f), 50.0f);
    addActionArea(TEXT("aim"), FVector(-130.0f, 0.0f, 48.0f), 50.0f);
    addActionArea(TEXT("muzzle"), FVector(265.0f, 0.0f, 48.0f), 150.0f);
}

void ACannon1857::BeginPlay() {
    Super::BeginPlay();
    hideMesh(TEXT("m1857_sight"));
}

void ACannon1857::addActionArea(const FString &name, const FVector &loc, float radius) {

    class USphereComponent *sphere = CreateDefaultSubobject<USphereComponent>(*name);
    sphere->InitSphereRadius(radius);
    sphere->SetCollisionProfileName("Trigger");
    sphere->AttachToComponent(
            RootComponent,
            FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true)
    );
    sphere->SetRelativeLocation(loc);
    sphere->OnComponentBeginOverlap.AddDynamic(this, &ACannon1857::onActionOverlapBegin);
    sphere->OnComponentEndOverlap.AddDynamic(this, &ACannon1857::onActionOverlapEnd);

    action_areas.Add(name, sphere);
    active_characters.Add(name, nullptr);
}

void ACannon1857::onActionOverlapBegin(class UPrimitiveComponent *own_comp,
                                       class AActor *other_actor,
                                       class UPrimitiveComponent *other_comp,
                                       int32 other_body_index,
                                       bool from_sweep,
                                       const FHitResult &hit_result) {
    FString aa_name = own_comp->GetName();
    ARegularCharacter * character = Cast<ARegularCharacter>(other_actor);
    if (!active_characters.Contains(aa_name) || character == nullptr) return;
    active_characters[aa_name] = character;
    active_characters[aa_name]->setCannon(this);
}

void ACannon1857::onActionOverlapEnd(class UPrimitiveComponent *own_comp,
                                     class AActor *other_actor,
                                     class UPrimitiveComponent *other_comp,
                                     int32 other_body_index) {
    FString aa_name = own_comp->GetName();
    if (!active_characters.Contains(aa_name) || active_characters[aa_name] == nullptr ||
        active_characters[aa_name]->GetName() != other_actor->GetName())
        return;
    active_characters[aa_name]->setCannon(nullptr);
    active_characters[aa_name] = nullptr;
}

bool ACannon1857::characterIsInPosition(ARegularCharacter *character, FString position) {
    return active_characters.Contains(position) &&
           active_characters[position] != nullptr &&
           active_characters[position]->GetName() == character->GetName();
}


bool ACannon1857::liftTail() {
    if (mesh_transformation_underway || is_tail_up || !can_move) return false;
    addMeshRotTransform(TEXT("m1857_stock"), -5.0f, 0.0f, 0.0f, 0.06f, 0.0f);
    addMeshRotTransform(TEXT("m1857_sight"), 5.0f, 0.0f, 0.0f, 0.06f, 0.0f);
    addMeshLocTransform(TEXT("m1857_stock"), -1.0f, 0.0f, 7.5f, 0.06f, 0.0f);
    startMeshTransformation();
    is_tail_up = true;
    tail_acceleration = 0.0f;
    return true;
}

bool ACannon1857::lowerTail() {
    if (mesh_transformation_underway || !is_tail_up) return false;
    addMeshRotTransform(TEXT("m1857_stock"), 5.0f, 0.0f, 0.0f, 1.0f, 0.0f);
    addMeshRotTransform(TEXT("m1857_sight"), -5.0f, 0.0f, 0.0f, 1.0f, 0.0f);
    addMeshLocTransform(TEXT("m1857_stock"), 1.0f, 0.0f, -7.5f, 1.0f, 0.0f);
    startMeshTransformation();
    is_tail_up = false;
    return true;
}

FVector ACannon1857::turnCannon(float value) {
    if (!can_move) return FVector(0.0f, 0.0f, 0.0f);
    if (tail_acceleration < 5.0) tail_acceleration += 0.02 * std::abs(value);
    float angle = value * (-0.04f) * tail_acceleration;
    FVector start = action_areas[TEXT("move")]->GetComponentLocation();
    AddActorWorldRotation(FRotator(0.0f, angle, 0.0f));
    tail_moved = true;
    return action_areas[TEXT("move")]->GetComponentLocation() - start;
}

FVector ACannon1857::moveCannon(float value) {
    if (!can_move || std::abs(value) < 0.001) return FVector(0.0f, 0.0f, 0.0f);
    float dist = value * 0.3;
    FVector start = action_areas[TEXT("move")]->GetComponentLocation();
    FVector direction = FRotationMatrix(GetActorRotation()).GetScaledAxis(EAxis::X);
    AddActorWorldOffset(direction * dist);
    int before = (int) (meshes[TEXT("m1857_wheel")]->GetRelativeRotation().Clamp().Pitch) % 30;
    FQuat wheel_rot = FRotator((value / 1500.0f) * -360.0f, 0.0f, 0.0f).Quaternion();
    meshes[TEXT("m1857_wheel")]->AddRelativeRotation(wheel_rot);
    int after = (int) (meshes[TEXT("m1857_wheel")]->GetRelativeRotation().Clamp().Pitch) % 30;

    if ((before > 15 && after < 15) || (before < 15 && after > 15)) {
        std::cout << "squeak\n";
        UGameplayStatics::PlaySoundAtLocation(GetWorld(), sounds[TEXT("cue_wheel")], GetActorLocation(), 1.0f, 1.0f,
                                              0.0f);
    }

    return action_areas[TEXT("move")]->GetComponentLocation() - start;
}

void ACannon1857::Tick(float delta_time) {
    Super::Tick(delta_time);
    if (is_tail_up) {
        if (tail_moved) {
            tail_moved = false;
        } else {
            tail_acceleration = tail_acceleration * 0.995 * delta_time - 0.001 * delta_time;
            if (tail_acceleration < 0) tail_acceleration = 0;
        }
    }
    tickRecoil(delta_time);
    tickElevation(delta_time);
//    tickElevation(delta_time);
//    tickMarker(delta_time);
}

//void ACannon1857::tickCamera(float delta_time) {
//    if (!cam_changed) return;
//    if(active_cam == "aim") {
//        cam_changed = camera_locations.at(active_cam).goTo(camera, delta_time, FVector(sight_offset_x, 0.0f, sight_offset_z));
//    } else {
//        cam_changed = camera_locations.at(active_cam).goTo(camera, delta_time);
//    }
//}
//
//void ACannon1857::tickElevation(float delta_time) {
//    if (elevation_angular_velocity == 0) return;
//    screw_height_pct = FMath::Clamp(screw_height_pct + elevation_angular_velocity * delta_time * elevation_factor, -0.2f, 1.0f);
//
//    float screw_height = screw_height_pct * screw_height_total;
//    float rotation = screw_rotation_total * screw_height_pct;
//    float cannon_rotation_rad = UKismetMathLibrary::Atan(screw_height / cannon_d_trunnion);
//    cannon_rotation = FMath::RadiansToDegrees(cannon_rotation_rad);
//    sight_offset_z =  UKismetMathLibrary::Sin(cannon_rotation_rad) * - cannon_d_sight;
//    sight_offset_x =  (UKismetMathLibrary::Cos(cannon_rotation_rad) - 1) * cannon_d_sight;
//
//    GEngine->ClearOnScreenDebugMessages();
//    GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, FString::Printf(TEXT("%0.2f°"), cannon_rotation));
//
//    mesh_screw->SetRelativeRotation(FRotator(0.0f, rotation, 0.0f));
//    mesh_screw->SetRelativeLocation(FVector(21.0f, -0.5f, 124.0f - 1.0 * screw_height));
//    mesh_cannon->SetRelativeRotation(FRotator(cannon_rotation, 0.0f, 0.0f));
//    mesh_sight->SetRelativeLocation(FVector(9.5f + sight_offset_x, 0.0f, 164.0f + sight_offset_z));
//
//    changeMarker(delta_time);
//}
//
//void ACannon1857::tickMarker(float delta_time) {
//    if (marker_velocity == 0) return;
//    changeMarker(delta_time);
//}

void ACannon1857::tickRecoil(float delta_time) {
    if (!recoiling) return;
    recoil_velocity -= recoil_deceleration * delta_time;
    moveCannon(-recoil_velocity);
    if (recoil_velocity <= 0) {
        recoiling = false;
    }
}

void ACannon1857::tickElevation(float delta_time) {
    if (elevation_angular_velocity == 0) return;
    screw_height_pct = FMath::Clamp(screw_height_pct + elevation_angular_velocity * delta_time * elevation_factor, -0.2f, 1.0f);
    elevation_angular_velocity = elevation_angular_velocity * 0.95;
    if (std::abs(elevation_angular_velocity) < 0.005) elevation_angular_velocity = 0;
    float screw_height = screw_height_pct * el_scr_total_height;
    float cannon_rotation_rad = UKismetMathLibrary::Atan(screw_height / cannon_d_trunnion);
    float screw_rotation = el_scr_total_rotations * screw_height_pct;
    cannon_elevation = FMath::RadiansToDegrees(cannon_rotation_rad);
    std::cout << "Elevation: " << cannon_elevation << "° \n";
    meshes[TEXT("m1857_cannon")]->SetRelativeRotation(FRotator(cannon_elevation, 0.0f, 0.0f));
    meshes[TEXT("m1857_sight")]->SetRelativeRotation(FRotator(-cannon_elevation, 0.0f, 0.0f));
    meshes[TEXT("m1857_elevation_screw")]->SetRelativeLocation(FVector(19.0f, 0.0f, 23.0f - 1.0 * screw_height));
    meshes[TEXT("m1857_elevation_screw")]->SetRelativeRotation(FRotator(0.0f, screw_rotation, 0.0f));
}

//
//// Called to bind functionality to input
//void ACannon1857::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent) {
//    Super::SetupPlayerInputComponent(PlayerInputComponent);
//
//    InputComponent->BindAction("right_mouse", IE_Pressed, this, &ACannon1857::onStartAiming);
//    InputComponent->BindAction("1", IE_Pressed, this, &ACannon1857::changeCamToFront);
//    InputComponent->BindAction("2", IE_Pressed, this, &ACannon1857::changeCamToSide);
//    InputComponent->BindAction("3", IE_Pressed, this, &ACannon1857::changeCamToBack);
//    InputComponent->BindAction("4", IE_Pressed, this, &ACannon1857::changeCamToLimber);
//    InputComponent->BindAction("space", IE_Pressed, this, &ACannon1857::Fire);
//
//    InputComponent->BindAxis("up_down_arrows", this, &ACannon1857::onChangeElevation);
//}
//
//void ACannon1857::changeMarker(float delta_time) {
//    marker_height_pct = FMath::Clamp(marker_height_pct + marker_velocity * delta_time, 0.0f, 1.0f);
//    float marker_height = marker_height_pct * marker_height_max;
//    mesh_sight_marker->SetRelativeLocation(FVector(9.5f + sight_offset_x, 0.0f, 170.0f + marker_height + sight_offset_z));
//}
//
pair <FVector, FRotator> ACannon1857::getMuzzleLocRot() {
    float cannon_d_muzzle = 270.0f;
    FRotator muzzle_rotation = GetActorRotation();
    muzzle_rotation.Pitch += cannon_elevation;

    FVector muzzle_location = GetActorLocation() + muzzle_rotation.Vector() * cannon_d_muzzle;
    muzzle_location.Z += 50;

//    DrawDebugSphere(GetWorld(), muzzle_location, 20.0f, 10, FColor::Green, false, 10, 0, 1.0f);
//    DrawDebugSphere(GetWorld(), muzzle_location +  muzzle_rotation.Vector() * 200, 10.0f, 10, FColor::Red, false, 10, 0, 1.0f);
//    DrawDebugLine(GetWorld(), muzzle_location, muzzle_location +  muzzle_rotation.Vector() * 200, FColor::Red, false, 10, 0, 1.0f);

    return {muzzle_location, muzzle_rotation};
}

void ACannon1857::elevate(float value) {
    elevation_acceleration = elevation_acceleration * 0.6 + FMath::Clamp(value, -1.0f, 1.0f);
    elevation_angular_velocity += + elevation_acceleration;
    if (std::abs(elevation_angular_velocity) < 0.005) elevation_angular_velocity = 0;
}

//void ACannon1857::onChangeElevation(float value) {
//    float sight_val = 0;
//    float elevation_val = 0;
//
//    if (!cam_changed && active_cam == "back") elevation_val = value;
//    if (!cam_changed && active_cam == "aim") sight_val = value * -0.001;
//
//    elevation_acceleration = elevation_acceleration * 0.6  + FMath::Clamp(elevation_val, -1.0f, 1.0f);
//    elevation_angular_velocity = 0.95 * elevation_angular_velocity + elevation_acceleration;
//    if(std::abs(elevation_angular_velocity) < 0.005) elevation_angular_velocity = 0;
//
//    marker_acceleration = marker_acceleration * 0.9  + FMath::Clamp(sight_val, -1.0f, 1.0f);
//    marker_velocity = 0.8 * marker_velocity + marker_acceleration;
//    if(std::abs(marker_velocity) < 0.0005) marker_velocity = 0;
//}

void ACannon1857::fire() {
    auto muzzle_loc_rot = getMuzzleLocRot();

    UWorld *world = GetWorld();
//    TSubclassOf<class AShell12Normal> projectile_class;
    if (world) {
        FActorSpawnParameters spawn_params;
        spawn_params.Owner = this;
        AShell12Normal * projectile = world->SpawnActor<AShell12Normal>(muzzle_loc_rot.first, muzzle_loc_rot.second, spawn_params);
        projectile->velocity = charge_velocity;
        if (projectile) {
            unblockMovement();
            removeStrikeMatch();
            emmitMuzzleParticles();
            addRecoil();
            playShotSound();
            FVector launch_direction = muzzle_loc_rot.second.Vector();
            projectile->FireInDirection(launch_direction);
            GEngine->ClearOnScreenDebugMessages();
            GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Fire!"));
        }
    }
}

void ACannon1857::emmitMuzzleParticles() {
    if (particles.Contains(TEXT("P_c_muzzle_flash"))) {
        FRotator rotation = GetActorRotation();
        FVector location = FVector(0.0f, 0.0f, 50.0f) + GetActorLocation() + rotation.Vector() * 110.0f;
        rotation += FRotator(0.0f, 90.0f, 0.0f);
        UParticleSystemComponent *particle = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), particles[TEXT(
                "P_c_muzzle_flash")], location, rotation, true, EPSCPoolMethod::None, true);
        particle->ActivateSystem();
        addMeshRotTransform(TEXT("m1857_cannon"), -10.0f, 0.0f, 0.0f, 2.0f, 0.0f);
        addMeshRotTransform(TEXT("m1857_cannon"), 10.0f, 0.0f, 0.0f, 0.2f, 0.2f);
        startMeshTransformation();
    }
}


void ACannon1857::addRecoil() {
    recoiling = true;
    recoil_velocity = initial_recoil_velocity;
}

void ACannon1857::playShotSound() {
    FVector location = getMuzzleLocRot().first;
    UGameplayStatics::PlaySoundAtLocation(GetWorld(), sounds[TEXT("cue_explosion")], location, 1.0f, 1.0f, 0.0f);
}


void ACannon1857::addSight() {
    showMesh(TEXT("m1857_sight"));
}

void ACannon1857::removeSight() {
    hideMesh(TEXT("m1857_sight"));
}

void ACannon1857::addStrikeMatch() {
    showMesh(TEXT("m1857_strike_pin"));
}

void ACannon1857::removeStrikeMatch() {
    hideMesh(TEXT("m1857_strike_pin"));
}

TPair <FVector, FRotator> ACannon1857::getSightLocRot() {
    FRotator rotation = GetActorRotation();
    FVector location = rotation.Vector() * -122.0f + FVector(0.0f, 0.0f, 71.0f);

//    DrawDebugSphere(GetWorld(), location + GetActorLocation(), 2.0f, 10, FColor::Green, false, 30, 0, 1.0f);
//    DrawDebugLine(GetWorld(), location+ GetActorLocation(), location+ GetActorLocation()  + rotation.Vector() * 40, FColor::Green, false, 30, 0, 1.0f);

    return TPair<FVector, FRotator>(location + GetActorLocation(), rotation + GetActorRotation());
}
