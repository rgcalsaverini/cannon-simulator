#pragma once

#include <map>
#include <string>
#include "BaseActor.h"
#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "CameraDirector.h"
#include "Misc/DateTime.h"
#include "Cannon1857.generated.h"

using std::pair;
using std::map;
using std::string;

class ARegularCharacter;

UCLASS()
class CANNON_API ACannon1857 : public ABaseActor {
    GENERATED_BODY()

    public:
        // Sets default values for this pawn's properties
        ACannon1857();

        bool characterIsInPosition(ARegularCharacter* character, FString position);

        bool liftTail();

        bool lowerTail();

        bool isTailUp() { return is_tail_up; }

        FVector turnCannon(float value);

        FVector moveCannon(float value);

        void fire();

        void blockMovement() { can_move = false; }

        void unblockMovement() { can_move = true; }

        bool sightIsOn() { return meshes[TEXT("m1857_sight")]->IsVisible(); }

        void addSight();

        void removeSight();

        void addStrikeMatch();

        void removeStrikeMatch();

        void elevate(float value);

        TPair<FVector, FRotator> getSightLocRot();

protected:
        virtual void BeginPlay() override;

        virtual void Tick(float delta_time) override;

        void tickElevation(float delta_time);

        void tickRecoil(float delta_time);

        void addActionArea(const FString &name, const FVector &loc, float radius);

        UFUNCTION(BlueprintCallable, Category = "Physics")
        void onActionOverlapBegin(class UPrimitiveComponent* own_comp, class AActor* other_actor, class UPrimitiveComponent* other_comp, int32 other_body_index, bool from_sweep, const FHitResult& hit_result);

        UFUNCTION(BlueprintCallable, Category = "Physics")
        void onActionOverlapEnd(class UPrimitiveComponent* own_comp, class AActor* other_actor, class UPrimitiveComponent* other_comp, int32 other_body_index);

        pair<FVector, FRotator> getMuzzleLocRot();

        void emmitMuzzleParticles();

        void addRecoil();

        void playShotSound();

    protected:
        UPROPERTY(VisibleAnywhere)
        TMap<FString, class USphereComponent*> action_areas;

        TMap<FString, class ARegularCharacter*> active_characters;

        UPROPERTY(VisibleAnywhere)
        UParticleSystem* muzzle_particle_asset;

        UPROPERTY(VisibleAnywhere)
        UParticleSystemComponent* muzzle_particle;

        /* Tail */
        bool is_tail_up = false;
        float tail_acceleration = 1.0f;
        bool tail_moved = false;
        bool can_move = true;

        /* Elevation */
        float cannon_elevation = 0.0f;
        float elevation_acceleration = 0.0f;
        float elevation_angular_velocity = 0.0f;
        float screw_height_pct = 0;

        /* Elevation config */
        float el_scr_total_rotations = 360.0 * 4.0;
        float el_scr_total_height = 14.0f;
        float cannon_d_trunnion = 70.0f; // distance between the breech and the trunnion
        float elevation_factor = 0.0025f;

        /* Charge */
        float charge_velocity = 20000.0f;


/* Recoil */
        bool recoiling = false;
        float recoil_velocity = 0;
        float recoil_deceleration = 20;
        float initial_recoil_velocity = 20;


//        UStaticMeshComponent *mesh_stock;
//        UStaticMeshComponent *mesh_screw;
//        UStaticMeshComponent *mesh_cannon;
//        UStaticMeshComponent *mesh_sight;
//        UStaticMeshComponent *mesh_sight_marker;
//        UCameraComponent *camera;
//
//        map<string,CameraDirector> camera_locations;
//
//        // Positions
//        bool cam_changed = false;
//        string active_cam;
//
//        // General cannon properties
//        float screw_rotation_total = 360 * 4;
//        float screw_height_total = 12;
//        float cannon_d_trunnion = 70.0f; // distance between the trunnion and the tip of the cannon
//        float cannon_d_sight = 82.0f; // distance between the trunnion and the sight
//        float cannon_d_muzzle = 140.0f; // distance between the trunnion and the muzzle
//
//
//        // Elevation
//        float elevation_factor = 0.002f;
//        float elevation_angular_velocity = 0;
//        float elevation_acceleration = 0;
//        float screw_height_pct = 0;
//        float cannon_rotation = 0;
//
//        // Sight
//        float sight_offset_z = 0;
//        float sight_offset_x = 0;
//        float marker_height_pct = 0;
//        float marker_height_max = 24;
//        float marker_acceleration = 0;
//        float marker_velocity = 0;
//
//        // Recoil
//        bool recoiling = false;
//        float recoil_velocity = 0;
//        float recoil_acceleration = 50;
//        float recoil_deceleration = 8;

};

/*
Cord: https://docs.unrealengine.com/en-US/API/Plugins/CableComponent/UCableComponent/index.html

 */