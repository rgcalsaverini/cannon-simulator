#include "FieldManualActor.h"
//right_left_arrows

AFieldManualActor::AFieldManualActor() : ABaseActor(TEXT("/Game/cannon/manual/")) {
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
    addMesh(TEXT("manual_half"));
    addMesh(TEXT("manual_spine"));
    addMesh(TEXT("manual_half_2"), FVector(0.0f, -2.1f, 0.72f), FRotator(0.0f, 0.0f, 180.0f));

    page_materials.Add(ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/cannon/manual/manual_mat_pg1.manual_mat_pg1")).Object);
    page_materials.Add(ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/cannon/manual/manual_mat_pg2.manual_mat_pg2")).Object);
    page_materials.Add(ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/cannon/manual/manual_mat_pg3.manual_mat_pg3")).Object);
    page_materials.Add(ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/cannon/manual/manual_mat_pg4.manual_mat_pg4")).Object);
}

void AFieldManualActor::open() {
    setPage(0);

    addMeshRotTransform(TEXT("manual_half"), 90.0f, 0.0f, 0.0f, 0.06f, 0.5f);
    addMeshLocTransform(TEXT("manual_half"), 0.0f, 0.0f, -12.0f, 0.06f, 0.5f);

    addMeshRotTransform(TEXT("manual_half_2"), 90.0f, 0.0f, 0.0f, 0.06f, 0.5f);
    addMeshLocTransform(TEXT("manual_half_2"), 0.0f, 00.0f, 12.0f, 0.06f, 0.5f);

    addMeshLocTransform(TEXT("manual_spine"), -11.9f, 0.0f, 0.0f, 0.06f, 0.5f);

    startMeshTransformation();
}

void AFieldManualActor::close() {
    addMeshRotTransform(TEXT("manual_half"), -90.0f, 0.0f, 0.0f, 0.2f, 0.0f);
    addMeshLocTransform(TEXT("manual_half"), 0.0f, 0.0f, 12.0f, 0.2f, 0.0f);

    addMeshRotTransform(TEXT("manual_half_2"), -90.0f, 0.0f, 0.0f, 0.2f, 0.0f);
    addMeshLocTransform(TEXT("manual_half_2"), 0.0f, 00.0f, -12.0f, 0.2f, 0.0f);

    addMeshLocTransform(TEXT("manual_spine"), 11.9f, 0.0f, 0.0f, 0.2f, 0.0f);

    startMeshTransformation();
}

void AFieldManualActor::setPage(int offset) {
//    https://unrealcpp.com/change-material-mesh/
//    ConstructorHelpers::FObjectFinder<UMaterial> base_mat(TEXT("/Game/cannon/manual/manual_base.manual_base"));
//    UMaterialInstanceDynamic* left_mat = UMaterialInstanceDynamic::Create(base_mat.Object, this);
    int new_page = current_page;
    if (offset > 0) {
        new_page = current_page + 2;
    } else if(offset < 0) {
        new_page = current_page - 2;
    }
    if(new_page < 0 || new_page >= page_materials.Num()) return;

    current_page = new_page;

    std::cout << "current page:" << current_page << "\n";
    meshes[TEXT("manual_half")]->SetMaterial(0, page_materials[current_page]);
    meshes[TEXT("manual_half_2")]->SetMaterial(0, page_materials[current_page + 1]);
}
