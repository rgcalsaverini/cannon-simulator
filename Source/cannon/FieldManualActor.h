#pragma once

#include "CoreMinimal.h"
#include "BaseActor.h"
#include "FieldManualActor.generated.h"

/**
 * 
 */
UCLASS()
class CANNON_API AFieldManualActor : public ABaseActor {
	GENERATED_BODY()
public:
    AFieldManualActor();

	void setPage(int offset);

	void open();

	void close();

protected:
    UPROPERTY(EditAnywhere)
    TArray<UMaterial*> page_materials;
    int current_page = 0;
};
