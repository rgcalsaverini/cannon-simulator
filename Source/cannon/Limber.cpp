#include "Limber.h"

ALimber::ALimber() : ABaseActor(TEXT("/Game/cannon/limber/")) {
    PrimaryActorTick.bCanEverTick = true;

    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

    setupLidCollisionSphere();

    colliding_actor = TEXT("");

    addMesh(TEXT("limber_carriage"));
    addMesh(TEXT("limber_box"), FVector(39.5f, 0.0f, 129.2f));
    addMesh(TEXT("limber_lid_front"), FVector(-6.9f, 1.0f, 139.7f));
    addMesh(TEXT("limber_lid_top"), FVector(84.7f, 0.0f, 176.3f));
    addMesh(TEXT("limber_lid_lock"), FVector(-95.61, 55.7f, 0.7f), TEXT("limber_lid_top"));
    addMesh(TEXT("limber_lock_bar"), FVector(-7.8f, -62.215f, 167.06f));
    addMesh(TEXT("limber_wheel"));
    addMesh(TEXT("limber_inside"), TEXT("limber_box"));

    hideMesh(TEXT("limber_inside"));
//    manual = GetWorld()->SpawnActor<AFieldManualActor>();
//    FActorSpawnParameters spawn_params;
//    spawn_params.Owner = this;
//    manual = GetWorld()->SpawnActor<AFieldManualActor>(FVector(0.0f, 0.0f, 0.0f), FRotator(0.0f, 0.0f, 0.0f), spawn_params);
}

void ALimber::setupLidCollisionSphere() {
    lid_collision_sphere = CreateDefaultSubobject<USphereComponent>(TEXT("LidCollision"));
    lid_collision_sphere->InitSphereRadius(lid_collision_radius);
    lid_collision_sphere->SetCollisionProfileName("Trigger");
    lid_collision_sphere->AttachToComponent(
            RootComponent,
            FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true)
    );
    lid_collision_sphere->SetRelativeLocation(FVector(-35.0f, 0.0f, 130.0f));
    lid_collision_sphere->OnComponentBeginOverlap.AddDynamic(this, &ALimber::onOverlapBegin);
    lid_collision_sphere->OnComponentEndOverlap.AddDynamic(this, &ALimber::onOverlapEnd);
}

void ALimber::onOverlapBegin(class UPrimitiveComponent *own_comp,
                             class AActor *other_actor,
                             class UPrimitiveComponent *other_comp,
                             int32 other_body_index,
                             bool from_sweep,
                             const FHitResult &hit_res) {
    if (!colliding_actor.IsEmpty() || Cast<ACharacter>(other_actor) == nullptr) {
        return;
    }
    colliding_actor = other_actor->GetName();
}

void ALimber::onOverlapEnd(class UPrimitiveComponent *own_comp,
                           class AActor *other_actor,
                           class UPrimitiveComponent *other_comp,
                           int32 other_body_index) {
    if (other_actor->GetName() != colliding_actor || Cast<ACharacter>(other_actor) == nullptr) {
        return;
    }
    colliding_actor = TEXT("");
    if (is_open && !mesh_transformation_underway) close();
}


void ALimber::BeginPlay() {
    Super::BeginPlay();
}

//void ALimber::Restart() {
//    Super::Restart();
//    ALimber * controlled_limber = Cast<ALimber>(GetController()->GetPawn());
//    if (controlled_limber != nullptr) {
////        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, TEXT("Possessed limber "));
//    }
//}

void ALimber::Tick(float delta_time) {
    Super::Tick(delta_time);
    if (is_hiding_interior) {
        hide_interior_timer -= delta_time;
        if (hide_interior_timer <= 0) {
            if (manual != nullptr) manual->Destroy();
            hideMesh(TEXT("limber_inside"));
            is_hiding_interior = false;
        }
    }
    tickManualMovement(delta_time);
}

void ALimber::handleExit() {
    colliding_actor = TEXT("");
    if (is_open && !mesh_transformation_underway) close();
//    APlayerController *player_controller = GetWorld()->GetFirstPlayerController();
//    player_controller->UnPossess();
//    player_controller->Possess(active_character);
}

bool ALimber::canOpen(ARegularCharacter *who) {
    return !mesh_transformation_underway && !is_open && who->GetName() == colliding_actor;
}

bool ALimber::canClose(ARegularCharacter *who) {
    return !mesh_transformation_underway && is_open && who->GetName() == colliding_actor;
}

bool ALimber::canOpenManual(ARegularCharacter *who) {
    return manual != nullptr &&
           active_character != nullptr &&
           !move_manual &&
           !manual_open &&
           who->GetName() == active_character->GetName();
}

bool ALimber::canCloseManual(ARegularCharacter *who) {
    return manual != nullptr &&
           active_character != nullptr &&
           !move_manual &&
           manual_open &&
           who->GetName() == active_character->GetName();
}

void ALimber::open(ARegularCharacter *previous) {
    showMesh(TEXT("limber_inside"));
    manual = GetWorld()->SpawnActor<AFieldManualActor>();
    manual->SetActorLocation(manual_initial_loc);
    manual->SetActorRotation(manual_initial_rot);
    manual->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepRelative, true), TEXT(""));
    active_character = previous;
    addMeshRotTransform(TEXT("limber_lid_lock"), -40.0f, 0.0f, 0.0f, 0.5f, 0.0f);
    addMeshRotTransform(TEXT("limber_lid_top"), -80.0f, 0.0f, 0.0f, 0.07f, 0.18f);
    addMeshRotTransform(TEXT("limber_lid_lock"), 120.0f, 0.0f, 0.0f, 0.7f, 0.18f);
    addMeshRotTransform(TEXT("limber_lock_bar"), 0.0f, 200.0f, 0.0f, 0.04f, 0.18f);
    addMeshRotTransform(TEXT("limber_lid_front"), 160.0f, 0.0f, 0.0f, 0.04f, 0.3f);
    startMeshTransformation();
    is_open = true;
}

void ALimber::close() {
    addMeshRotTransform(TEXT("limber_lid_front"), -160.0f, 0.0f, 0.0f, 0.06f, 0.0f);
    addMeshRotTransform(TEXT("limber_lock_bar"), 0.0f, -200.0f, 0.0f, 0.04f, 0.22f);
    addMeshRotTransform(TEXT("limber_lid_top"), 80.0f, 0.0f, 0.0f, 0.07f, 1.0f);
    addMeshRotTransform(TEXT("limber_lid_lock"), -120.0f, 0.0f, 0.0f, 0.07f, 1.0f);
    addMeshRotTransform(TEXT("limber_lid_lock"), 40.0f, 0.0f, 0.0f, 0.05f, 1.6f);
    startMeshTransformation();
    active_character = nullptr;
    is_open = false;
    is_hiding_interior = true;
    hide_interior_timer = 2;
    if (manual_open) closeManual();
}

void ALimber::openManual(const FVector &location, const FRotator &rotation) {
    if (manual == nullptr || move_manual || active_character == nullptr || !is_open || colliding_actor.IsEmpty()) return;

    GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, TEXT("Open manual"));

    FTransform transform;
    transform.SetRotation(rotation.Quaternion());
    transform.ConcatenateRotation(FRotator(0.0f, 0.0f, 90.0f).Quaternion());
    transform.NormalizeRotation();

    const FVector direction = rotation.Quaternion().GetForwardVector();
    manual_target_rot = transform.GetRotation();
    manual_target_loc = location + direction * 35;
    manual->open();
    move_manual = true;
    manual_open = true;
    manual_transition = 0.0f;
}

void ALimber::setManualPage(int offset) {
    if (manual == nullptr || move_manual || active_character == nullptr || !is_open || colliding_actor.IsEmpty()) return;
    manual->setPage(offset);
}

void ALimber::closeManual() {
    if (manual == nullptr || move_manual || active_character == nullptr || !is_open || colliding_actor.IsEmpty()) return;

    GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Close manual"));

    manual->SetActorRotation(manual_initial_rot);
    manual_target_rot = (GetActorRotation() + manual->GetActorRotation()).Quaternion();
    manual_target_loc = GetActorLocation() + FVector(23.57f, -34.11f, 154.12f);
    manual->close();
    move_manual = true;
    manual_open = false;
    manual_transition = 0.0f;
}

void ALimber::tickManualMovement(float delta) {
    if (!move_manual) return;
    manual_transition += delta;
    /* Location */
    FVector current_loc = manual->GetActorLocation();
    FVector new_loc = FVector(
            FMath::FInterpTo(current_loc.X, manual_target_loc.X, manual_transition, 0.5),
            FMath::FInterpTo(current_loc.Y, manual_target_loc.Y, manual_transition, 0.5),
            FMath::FInterpTo(current_loc.Z, manual_target_loc.Z, manual_transition, 0.5)
    );

    manual->SetActorLocation(new_loc);

    /* Rotation */
    FQuat current_rot = manual->GetActorRotation().Quaternion();
    FQuat target_rot = manual_target_rot;
    current_rot.Normalize();
    target_rot.Normalize();
    std::cout << "Current: " << current_rot.X << ", " << current_rot.Y << ", " << current_rot.Z << ", " << current_rot.W << "\n";
    std::cout << "Target: " << target_rot.X << ", " << target_rot.Y << ", " << target_rot.Z << ", " << target_rot.W << "\n\n";

    FQuat new_rot = FQuat(
            FMath::FInterpTo(current_rot.X, target_rot.X, manual_transition, 0.05),
            FMath::FInterpTo(current_rot.Y, target_rot.Y, manual_transition, 0.05),
            FMath::FInterpTo(current_rot.Z, target_rot.Z, manual_transition, 0.05),
            FMath::FInterpTo(current_rot.W, target_rot.W, manual_transition, 0.05)
    );
    manual->SetActorRotation(new_rot);
//    manual->SetActorRotation(FMath::RInterpTo(manual->GetActorRotation(), manual_target_rot.Rotator(), delta, 0.05));

    move_manual = manual_transition < 3;
}