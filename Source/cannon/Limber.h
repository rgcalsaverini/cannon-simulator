#pragma once

#include <vector>
#include "characters/RegularCharacter.h"
#include "BaseActor.h"
#include "FieldManualActor.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Camera/CameraComponent.h"
#include "Components/SphereComponent.h"
#include "AnimationStateMachine.h"
#include "Limber.generated.h"

class ARegularCharacter;

using std::vector;

UCLASS()
class CANNON_API ALimber  : public ABaseActor {
    GENERATED_BODY()

public:
    // Sets default values for this pawn's properties
    ALimber();

    virtual void Tick(float DeltaTime) override;

    void open(ARegularCharacter* previous);

    void close();

    bool canOpen(ARegularCharacter *who);

    bool canClose(ARegularCharacter *who);

    bool canOpenManual(ARegularCharacter *who);

    bool canCloseManual(ARegularCharacter *who);

    UFUNCTION(BlueprintCallable, Category = "Physics")
    void onOverlapBegin(class UPrimitiveComponent* own_comp, class AActor* other_actor, class UPrimitiveComponent* other_comp, int32 other_body_index, bool from_sweep, const FHitResult& hit_result);

    UFUNCTION(BlueprintCallable, Category = "Physics")
    void onOverlapEnd(class UPrimitiveComponent* own_comp, class AActor* other_actor, class UPrimitiveComponent* other_comp, int32 other_body_index);

//    void toggleManual(const FVector &location, const FRotator &rotation);

    void openManual(const FVector &location, const FRotator &rotation);

    void setManualPage(int offset);

void closeManual();

    void tickManualMovement(float delta);

protected:
    virtual void BeginPlay() override;

    void handleExit();

    void setupLidCollisionSphere();

public:
    UPROPERTY(VisibleAnywhere)
    class USphereComponent* lid_collision_sphere;

protected:
    float lid_collision_radius = 20.0f;

    UPROPERTY(VisibleAnywhere)
    bool is_open;

    UPROPERTY(VisibleAnywhere)
    FString colliding_actor;

    bool is_hiding_interior = false;

    float hide_interior_timer = 0;

    ARegularCharacter *active_character;

    /*  Manual */
    UPROPERTY(VisibleAnywhere)
    AFieldManualActor* manual;

    UPROPERTY(VisibleAnywhere)
    FVector manual_initial_loc = FVector(23.57f, -34.11f, 154.12f);
    FRotator manual_initial_rot = FRotator(0.0f, 280.00f, 0.0f);
    FVector manual_target_loc;
    FQuat manual_target_rot;
    bool move_manual = false;
    bool manual_open = false;
    float manual_transition = 0;


};
