#include "DrawDebugHelpers.h"
#include "Shell12Normal.h"

// Sets default values
AShell12Normal::AShell12Normal() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
    CollisionComponent->InitSphereRadius(impact_radius);
    RootComponent = CollisionComponent;

    ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
    ProjectileMovementComponent->SetUpdatedComponent(CollisionComponent);
    ProjectileMovementComponent->bRotationFollowsVelocity = true;
    ProjectileMovementComponent->bShouldBounce = true;
    ProjectileMovementComponent->Bounciness = bounciness;
    CollisionComponent->BodyInstance.SetCollisionProfileName(TEXT("projectile"));

    ConstructorHelpers::FObjectFinder <UStaticMesh> asset(TEXT("/Game/cannon/shells/normal12.normal12"));
    projectile_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile"));

    if (asset.Succeeded()) {
        projectile_mesh->SetStaticMesh(asset.Object);
        projectile_mesh->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
        projectile_mesh->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));
        projectile_mesh->AttachToComponent(RootComponent, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true));
    }
}

// Called when the game starts or when spawned
void AShell12Normal::BeginPlay() {
	Super::BeginPlay();
    CollisionComponent->OnComponentHit.AddDynamic(this, &AShell12Normal::OnHit);
}

// Called every frame
void AShell12Normal::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	if(GetActorLocation().Z < -150 && !hit_the_ground) {
        FTimespan tof = FDateTime::Now() - fired_time;
        std::cout << "Distance: " << FVector::Dist(initial_location, GetActorLocation()) / 100.0 << "m \n";
        std::cout << "Time (s): " << tof.GetTotalSeconds() << "\n";
        std::cout << "Charge (s): " << velocity << "cm/s \n";
        hit_the_ground = true;
//        DrawDebugSphere(GetWorld(), GetActorLocation(), 20.0f, 10, FColor::Red, false, 6000, 0, 3.0f);
//	} else if(!hit_the_ground) {
//        DrawDebugSphere(GetWorld(), GetActorLocation(), 2.0f, 10, FColor::Green, false, 6000, 0, 1.0f);
	}
}

void AShell12Normal::FireInDirection(const FVector& ShootDirection, float speed_factor) {
    hit_the_ground = false;
    fired_time = FDateTime::Now();
    initial_location = GetActorLocation();
    ProjectileMovementComponent->InitialSpeed = velocity;
    ProjectileMovementComponent->MaxSpeed = velocity;
    ProjectileMovementComponent->Velocity = ShootDirection * ProjectileMovementComponent->InitialSpeed * speed_factor;
}

void AShell12Normal::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit) {
    if (OtherActor != this && OtherComponent->IsSimulatingPhysics() && impact_force >= 0.01) {
//        OtherComponent->AddImpulseAtLocation(ProjectileMovementComponent->Velocity * impact_force, Hit.ImpactPoint);
        OtherComponent->AddForceAtLocation(ProjectileMovementComponent->Velocity * impact_force, Hit.ImpactPoint, TEXT("None"));
    }
    impact_force *= 0.9;
}