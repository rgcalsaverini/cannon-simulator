// https://docs.unrealengine.com/en-US/Programming/Tutorials/FirstPersonShooter/3/1/index.html
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Misc/DateTime.h"
#include "Shell12Normal.generated.h"

UCLASS()
class CANNON_API AShell12Normal : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShell12Normal();

    void FireInDirection(const FVector& ShootDirection, float speed_factor=1);

    UFUNCTION(BlueprintCallable)
    void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	float lifespan = 60.0f;
	float impact_force = 10000.0f;
	float bounciness = 0.00001f;
	float impact_radius = 20.0f;

public:
    float velocity = 16000.0f;
	// Called every frame
	bool hit_the_ground = false;
    FDateTime fired_time;
    FVector initial_location;
	virtual void Tick(float DeltaTime) override;
    UStaticMeshComponent * projectile_mesh;

    UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
    USphereComponent* CollisionComponent;

    UPROPERTY(VisibleAnywhere, Category = Movement)
    UProjectileMovementComponent* ProjectileMovementComponent;
};
