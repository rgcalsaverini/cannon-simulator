#include "Cannon1857.h"
#include "Math/Vector.h"
#include "DrawDebugHelpers.h"
#include "StrikeMatchHandle.h"


AStrikeMatchHandle::AStrikeMatchHandle() : ABaseActor(TEXT("/Game/cannon/m1857/")) {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    addMesh(TEXT("SM_handle_fuse"), FVector(0.0f, 0.0f, 0.0f), FRotator(0.0f, 90.0f, 0.0f));
    cable_material = ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/cannon/m1857/material/M_cord.M_cord")).Object;
    cable = CreateDefaultSubobject<UCableComponent>(TEXT("Cable"));
    cable->CableWidth = 1.2f;
    cable->CableLength = 80.0f;
    cable->bAttachEnd = true;
    cable->bAttachStart = true;
    cable->bEnableStiffness = true;
    cable->SetMaterial(0, cable_material);
}

void AStrikeMatchHandle::setupCable(ACannon1857* new_cannon) {
    cannon = new_cannon;
    end_offset_inside_cannon = FVector(-63.0f, -0.9f, 70.0f);
    end_offset = FVector(0.0f, 0.0f, 65.0f) +  cannon->GetActorRotation().Vector() * -68.0f;
    cable->SetRelativeLocation(GetActorLocation());
    cable->SetAttachEndTo(Cast<AActor>(cannon), TEXT(""));
    cable->EndLocation = end_offset_inside_cannon;
    cannon->addStrikeMatch();
}


void AStrikeMatchHandle::BeginPlay() {
	Super::BeginPlay();
}

void AStrikeMatchHandle::EndPlay(EEndPlayReason::Type reason) {
	Super::EndPlay(reason);
	std::cout << "Bye strike match\n";
    if (cannon != nullptr) {
        std::cout << "Also, I have a cannon!\n";
        cannon->removeStrikeMatch();
    }
}

void AStrikeMatchHandle::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

void AStrikeMatchHandle::moveCableTo(FVector location) {
    if (cable == nullptr || cannon == nullptr) return;
    cable->SetRelativeLocation(location);
    FVector start = cable->GetAttachedComponent()->GetComponentLocation() + end_offset;
    if (cable->bAttachEnd == false) return;
    if (FVector::Dist(start, location) > 250) {
        cable->bAttachEnd = false;
        cannon->fire();
    }
}