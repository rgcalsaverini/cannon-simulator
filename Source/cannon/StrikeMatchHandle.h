// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseActor.h"
#include "CableComponent.h"
#include "StrikeMatchHandle.generated.h"

class ACannon1857;

UCLASS()
class CANNON_API AStrikeMatchHandle : public ABaseActor {
	GENERATED_BODY()

public:
    AStrikeMatchHandle();

    void setupCable(ACannon1857* cannon);

    void moveCableTo(FVector location);
protected:
    virtual void BeginPlay() override;

    virtual void EndPlay(EEndPlayReason::Type reason) override;

public:

    virtual void Tick(float DeltaTime) override;
protected:
    UPROPERTY(VisibleAnywhere)
    UCableComponent *cable = nullptr;
    UMaterial* cable_material;
    ACannon1857 *cannon;
    FVector end_offset_inside_cannon;
    FVector end_offset;
};
