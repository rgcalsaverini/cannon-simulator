#include "RegularCharacter.h"
#include "DrawDebugHelpers.h"
#include "Math/UnrealMathUtility.h"
#include "../Limber.h"
#include "../Utils.h"


ARegularCharacter::ARegularCharacter() {
    PrimaryActorTick.bCanEverTick = true;
    AutoPossessPlayer = EAutoReceiveInput::Player0;

    /* Setup camera */
    first_person_camera = CreateDefaultSubobject<UCineCameraComponent>(TEXT("FirstPersonCamera"));
    first_person_camera->SetupAttachment(RootComponent);
    first_person_camera->SetRelativeLocation(FVector(0.0f, 0.0f, 127.0f));
    first_person_camera->bUsePawnControlRotation = true;
    first_person_camera->FocusSettings.bSmoothFocusChanges = true;
    first_person_camera->SetCurrentFocalLength(13);
    first_person_camera->CurrentAperture = 22.0;
    first_person_camera->FocusSettings.ManualFocusDistance = 1000000.0;
}

void ARegularCharacter::BeginPlay() {
    Super::BeginPlay();
}

void ARegularCharacter::Tick(float delta_time) {
    Super::Tick(delta_time);
    tickDOF(delta_time);
}

void ARegularCharacter::tickDOF(float delta_time) {
    if (!tick_dof) return;
    dof_elapsed += delta_time;
    bool aperture_not_done = false;
    bool cam_focus_not_done = false;
    bool cam_focal_length_not_done = false;


    float current_aperture = first_person_camera->CurrentAperture;
    float new_aperture = FMath::FInterpTo(current_aperture, target_cam_aperture, dof_elapsed, 0.1);
    first_person_camera->CurrentAperture = new_aperture;
    aperture_not_done = std::abs(target_cam_aperture - new_aperture) > 0.1;

    float current_cam_focus = first_person_camera->FocusSettings.ManualFocusDistance;
    float new_cam_focus = FMath::FInterpTo(current_cam_focus, target_cam_focus, dof_elapsed, 0.1);
    first_person_camera->FocusSettings.ManualFocusDistance = new_cam_focus;
    cam_focus_not_done = std::abs(target_cam_focus - new_cam_focus) > 0.1;

    float current_cam_focal_length = first_person_camera->CurrentFocalLength;
    float new_cam_focal_length = FMath::FInterpTo(current_cam_focal_length, target_cam_focal_length, dof_elapsed, 0.5);
    first_person_camera->SetCurrentFocalLength(new_cam_focal_length);
    cam_focal_length_not_done = std::abs(target_cam_focal_length - new_cam_focal_length) > 0.1;

    tick_dof = aperture_not_done || cam_focus_not_done || cam_focal_length_not_done;
}


void ARegularCharacter::SetupPlayerInputComponent(UInputComponent *player_input_component) {
    Super::SetupPlayerInputComponent(player_input_component);
    /* Movement */
    player_input_component->BindAxis("move_forward_backward", this, &ARegularCharacter::moveForward);
    player_input_component->BindAxis("move_right_left", this, &ARegularCharacter::moveRight);
    player_input_component->BindAxis("look_right_left", this, &ARegularCharacter::handleTurnHorizontal);
    player_input_component->BindAxis("look_up_down", this, &ARegularCharacter::handleTurnVertical);

    /* Interaction */
    player_input_component->BindAction("left", IE_Pressed, this, &ARegularCharacter::handleLeftArrow);
    player_input_component->BindAction("right", IE_Pressed, this, &ARegularCharacter::handleRightArrow);
    player_input_component->BindAction("top", IE_Pressed, this, &ARegularCharacter::handleRightArrow);
    player_input_component->BindAction("bottom", IE_Pressed, this, &ARegularCharacter::handleRightArrow);
    player_input_component->BindAction("interact", IE_Pressed, this, &ARegularCharacter::interactWithObject);
    player_input_component->BindAction("interact2", IE_Pressed, this,
                                       &ARegularCharacter::alternativeInteractWithObject);
    player_input_component->BindAction("aim", IE_Pressed, this, &ARegularCharacter::handleStartAiming);
    player_input_component->BindAction("aim", IE_Released, this, &ARegularCharacter::handleStopAiming);
    player_input_component->BindAction("left_mouse", IE_Pressed, this, &ARegularCharacter::handleClick);
    player_input_component->BindAxis("up_down_arrows", this, &ARegularCharacter::handleTopDownArrows);

}

void ARegularCharacter::moveForward(float value) {
    if (!value || is_using_sight) return;
    if (is_moving_cannon && cannon) {
        FVector new_loc = cannon->moveCannon(value);
        AddActorWorldOffset(new_loc);
        return;
    }
    FVector direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
    AddMovementInput(direction, value * 0.5);
    if (has_strike_match && strike_match != nullptr) strike_match->moveCableTo(strike_match->GetActorLocation());

}

void ARegularCharacter::moveRight(float value) {
    if (!value || is_using_sight) return;

    if (is_moving_cannon && cannon) {
        FVector new_loc = cannon->turnCannon(value);
        AddActorWorldOffset(new_loc);
        return;
    }
    FVector direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
    AddMovementInput(direction, value * 0.5);
    if (has_strike_match && strike_match != nullptr) strike_match->moveCableTo(strike_match->GetActorLocation());
}

void ARegularCharacter::handleLeftArrow() {
    if (manual_open && limber != nullptr) limber->setManualPage(-2);
}

void ARegularCharacter::handleRightArrow() {
    if (manual_open && limber != nullptr) limber->setManualPage(2);
}

void ARegularCharacter::setCloseDepthOfField() {
    target_cam_aperture = 4.0f;
    target_cam_focus = 35.0f;
    dof_elapsed = 0.0f;
    tick_dof = true;
}

void ARegularCharacter::setNormalDepthOfField() {
    target_cam_aperture = 22.0f;
    target_cam_focus = 1000000.0f;
    dof_elapsed = 0.0f;
    tick_dof = true;
}

void ARegularCharacter::setFarDepthOfField() {
    target_cam_aperture = 4.0f;
    target_cam_focus = 1000000.0f;
    dof_elapsed = 0.0f;
    tick_dof = true;
}

void ARegularCharacter::handleStartAiming() {
    if (cannon != nullptr && cannon->characterIsInPosition(this, TEXT("aim")) && cannon->sightIsOn()) {
        if (is_using_sight) {
            first_person_camera->SetRelativeLocation(cam_last_loc);
            first_person_camera->SetRelativeRotation(cam_last_rot);
            setNormalDepthOfField();
            is_using_sight = false;
            target_cam_focal_length = 13;
            target_cam_focus = 1000000.0f;
            target_cam_aperture = 22.0f;
            dof_elapsed = 0.0f;
            tick_dof = true;
        } else {
            cam_last_loc = first_person_camera->GetRelativeLocation();
            cam_last_rot = first_person_camera->GetRelativeRotation();
            TPair <FVector, FRotator> loc_rot = cannon->getSightLocRot();
            first_person_camera->SetWorldLocation(loc_rot.Key);
//            setCloseDepthOfField();
            is_using_sight = true;
            target_cam_focal_length = 22.0f;
            dof_elapsed = 0.0f;
            tick_dof = true;
        }
        return;
    }
    target_cam_focal_length = 22.0f;
    dof_elapsed = 0.0f;
    tick_dof = true;
}

void ARegularCharacter::handleStopAiming() {
    if (cannon != nullptr && cannon->characterIsInPosition(this, TEXT("aim")) && cannon->sightIsOn()) {
        return;
    }
    target_cam_focal_length = 13;
    dof_elapsed = 0.0f;
    tick_dof = true;
}

void ARegularCharacter::handleTurnHorizontal(float value) {
    float max_rot = 15;
    float val_multiplier = 0.1;

    if (manual_open) {
        value *= val_multiplier;
        float current_yaw = (GetActorRotation() + first_person_camera->GetRelativeRotation()).Clamp().Yaw;
        float target_yaw = anchor_cam_rot.Clamp().Yaw;

        if (value > 0 && current_yaw - target_yaw > max_rot) {
            value = 0;
        } else if (value < 0 && current_yaw - target_yaw < -max_rot) {
            value = 0;
        }
        AddControllerYawInput(value);
        return;
    }
    if (has_strike_match && strike_match != nullptr) strike_match->moveCableTo(strike_match->GetActorLocation());
    AddControllerYawInput(value);
}

void ARegularCharacter::handleTurnVertical(float value) {
    float max_rot = 15;
    float val_multiplier = 0.1;

    if (manual_open) {
        value *= val_multiplier;
        float current_pitch = (GetActorRotation() + first_person_camera->GetRelativeRotation()).Clamp().Pitch;
        float target_pitch = anchor_cam_rot.Clamp().Pitch;
        std::cout << "current: " << current_pitch << " camera: " << target_pitch << "\n";

        if (value > 0 && target_pitch - current_pitch > max_rot) {
            return;
        } else if (value < 0 && target_pitch - current_pitch < -max_rot) {
            return;
        }
        AddControllerPitchInput(value);
        return;
    }
    AddControllerPitchInput(value);
}

void ARegularCharacter::interactWithObject() {
    if (has_strike_match) {
        dropStrikeMatch();
    } else if (cannon != nullptr) {
        handleInteractWithCannon();
    } else {
        interactRay();
    }
}

void ARegularCharacter::alternativeInteractWithObject() {
    if (cannon != nullptr && cannon->characterIsInPosition(this, TEXT("aim")) && !has_strike_match) {
        if (cannon->sightIsOn() && !is_using_sight) {
            cannon->removeSight();
            has_sight = true;
        } else if (has_sight) {
            cannon->addSight();
            has_sight = false;
        }
    } else if (limber != nullptr) {
        if (limber->canOpenManual(this)) {
            anchor_cam_loc = first_person_camera->GetRelativeLocation() + GetActorLocation();
            anchor_cam_rot = first_person_camera->GetRelativeRotation() + GetActorRotation();
            limber->openManual(anchor_cam_loc, anchor_cam_rot);
            setCloseDepthOfField();
            manual_open = true;
        } else if (limber->canCloseManual(this)) {
            setNormalDepthOfField();
            limber->closeManual();
            manual_open = false;
        }
    } else if (cannon != nullptr) {
        cannon->fire();
    }
}

void ARegularCharacter::interactRay() {
    FHitResult out_hit;
    FVector start = first_person_camera->GetComponentLocation();
    FVector forward_vector = first_person_camera->GetForwardVector();
    FVector end = ((forward_vector * interact_distance) + start);
    FCollisionQueryParams collision_params;
    bool hit = false;

    if (GetWorld()->LineTraceSingleByChannel(out_hit, start, end, ECC_Visibility, collision_params)) {
        if (out_hit.bBlockingHit) {
            if (GEngine) {
                AActor *hit_actor = out_hit.GetActor();
                if (handleInteractWithLimber(hit_actor)) {
                    hit = true;
//                    DrawDebugLine(GetWorld(), start, out_hit.ImpactPoint, FColor::Green, false, 10, 0, 0.5);
//                    DrawDebugSphere(GetWorld(), out_hit.ImpactPoint, 2.0f, 3, FColor::Green, false, 10, 0, 1.0f);
                }
            }
        }
    }
//    if (!hit) {
//        DrawDebugLine(GetWorld(), start, end, FColor::Red, false, 10, 0, 0.5);
//    }
}

void ARegularCharacter::setCannon(ACannon1857 *target) {
    if (target == nullptr && cannon) {
        cannon->lowerTail();
        is_moving_cannon = false;
    }
    cannon = target;
}

void ARegularCharacter::handleInteractWithCannon() {
    if (cannon == nullptr) return;

    if (cannon->characterIsInPosition(this, TEXT("move"))) {
        if (cannon->isTailUp() && cannon->lowerTail()) {
            is_moving_cannon = false;
        } else if (cannon->liftTail()) {
            is_moving_cannon = true;
        }
    } else if (cannon->characterIsInPosition(this, TEXT("aim")) && !has_strike_match && !is_using_sight) {
        wieldStrikeMatch();
    }
}

bool ARegularCharacter::handleInteractWithLimber(AActor *target) {
    limber = Cast<ALimber>(target);

    if (limber != nullptr) {
        if (limber->canOpen(this)) {
            limber->open(this);
        } else if (limber->canClose(this)) {
            limber->close();
            limber = nullptr;
        }
        return true;
    }

    return false;
}

void ARegularCharacter::wieldStrikeMatch() {
    if (cannon == nullptr) return;
    has_strike_match = true;
    cannon->blockMovement();
    FVector location = FVector(60.0f, 0.0f, 90.0f);
    strike_match = GetWorld()->SpawnActor<AStrikeMatchHandle>();
    strike_match->SetActorLocation(location);
    strike_match->moveCableTo(location);
    strike_match->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepRelative, true), TEXT(""));
    strike_match->setupCable(cannon);
}

void ARegularCharacter::dropStrikeMatch() {
    if (strike_match == nullptr) return;
    if (cannon != nullptr) cannon->unblockMovement();

    has_strike_match = false;
    strike_match->Destroy();
    strike_match = nullptr;

}

void ARegularCharacter::handleClick() {
    if (!is_using_sight) return;
    if (view_near) {
        setFarDepthOfField();
        view_near = false;
    } else {
        setCloseDepthOfField();
        view_near = true;
    }
}

void ARegularCharacter::handleTopDownArrows(float value) {
    if (value == 0) return;
    if (cannon != nullptr && cannon->characterIsInPosition(this, TEXT("aim")) && !has_strike_match && !is_using_sight) {
        cannon->elevate(value);
    }
}
