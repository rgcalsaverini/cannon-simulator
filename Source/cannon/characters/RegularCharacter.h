// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../Cannon1857.h"
#include "../StrikeMatchHandle.h"
#include "CineCameraComponent.h"
#include "RegularCharacter.generated.h"

class ALimber;

class ACannon1857;

UCLASS()
class CANNON_API ARegularCharacter : public ACharacter  {
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARegularCharacter();

    virtual void Tick(float DeltaTime) override;

    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    void setCannon(ACannon1857* target);

protected:

    void tickDOF(float delta_time);

	virtual void BeginPlay() override;

	/* Movement */
    UFUNCTION()
    void moveForward(float value);

    UFUNCTION()
    void moveRight(float value);

    /* Interaction */
    void interactWithObject();

    void alternativeInteractWithObject();

    void interactRay();

    bool handleInteractWithLimber(AActor *actor);

    void handleInteractWithCannon();

    void handleLeftArrow();

    void handleRightArrow();

    void handleTopDownArrows(float value);

    /* Camera */

    void setCloseDepthOfField();

    void setFarDepthOfField();

    void setNormalDepthOfField();

    void handleTurnHorizontal(float value);

    void handleTurnVertical(float value);

    void handleStartAiming();

    void handleStopAiming();

    void handleClick();

    /* Wielding stuff */

    void wieldStrikeMatch();

    void dropStrikeMatch();

private:
    UPROPERTY(VisibleAnywhere)
    UCineCameraComponent* first_person_camera;

    UPROPERTY(VisibleAnywhere)
    float interact_distance = 180.0f;

    UPROPERTY(VisibleAnywhere)
    ALimber *limber;

    UPROPERTY(VisibleAnywhere)
    ACannon1857 *cannon;

    UPROPERTY(VisibleAnywhere)
    bool has_sight = true;

    /* State */
    bool manual_open = false;
    bool is_moving_cannon = false;
    FRotator anchor_cam_rot;
    FVector anchor_cam_loc;

    /* Depth of Field */
    float target_cam_aperture = 22.0f;
    float target_cam_focus = 1000000.0f;
    float target_cam_focal_length = 14.0f;
    float dof_elapsed = 0.0f;
    bool tick_dof = false;

    /* Strike match */
    bool has_strike_match = false;
    UPROPERTY(VisibleAnywhere)
    AStrikeMatchHandle* strike_match;

    /* Aim */
    FVector cam_last_loc;
    FRotator cam_last_rot;
    bool is_using_sight = false;
    bool view_near = true;
};
